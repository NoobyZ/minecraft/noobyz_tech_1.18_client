// priority: 0

console.info('Write Information for JEI Items!')

onEvent('jei.information', event => {

    // Minecraft
    event.add('minecraft:wither_rose',[
        'Spawns when a mob is killed by Withering.'
    ])
    event.add('minecraft:nautilus_shell', [
        'Obtained by killing Nautilus, found in Warm Oceans.',
        ' ',
        'Also obtained from Embedded Ammonite, found at any Y level in Oceans and Beaches.'
    ])
    event.add('minecraft:dragon_egg', [
        'More eggs may be obtained by summoning the End Dragon again.',
        ' ',
        'To summon, place an End Crystal on each of the cardinal directions of the End Portal in the End.'
    ])
    event.add(/sophisticatedbackpacks:.*backpack/, [
        'Upgradable by placing it (Shift+Right Click) and Right Clicking it with the items mentioned on its tooltip.'
    ])
/*
    // Refined Storage
    event.add('refinedstorage:speed_upgrade', [
        'Makes devices faster.',
        'Can be installed to:',
        '• Importer',
        '• Exporter',
        '• Constructor',
        '• Destructor',
        '• Interface',
        '• Crafter',
        '• Disk Manipulator'
    ])
    event.add('refinedstorage:range_upgrade', [
        'Increases the range of the Wireless Transmitter.'
    ])
    event.add('refinedstorage:stack_upgrade', [
        'Makes devices work in stacks instead of individual items.',
        'Can be installed to:',
        '• Importer',
        '• Exporter',
        '• Constructor (dropping)',
        '• Interface',
        '• Disk Manipulator'
    ])
    event.add('refinedstorage:crafting_upgrade', [
        'Makes devices schedule a crafting task if the item is not available.',
        'Can be installed to:',
        '• Exporter',
        '• Constructor'
    ])
    event.add('refinedstorage:regulator_upgrade', [
        'Makes the Exporter stop outputting when the target inventory has the specified amount of the item.',
        'For instance, an Exporter set to export 32 of an item will stop exporting when the inventory has 32 of that item.'
    ])
    event.add('refinedstorage:fortune_1_upgrade', [
        'Enchants the Destructor with Fortune 1.'
    ])
    event.add('refinedstorage:fortune_2_upgrade', [
        'Enchants the Destructor with Fortune 2.'
    ])
    event.add('refinedstorage:fortune_3_upgrade', [
        'Enchants the Destructor with Fortune 3.'
    ])
    event.add('refinedstorage:silk_touch_upgrade', [
        'Enchants the Destructor with Silk Touch.'
    ])
    event.add('refinedstorage:cover', [
        'For other Covers, replace the stone with a block you want.'
    ])
*/
    // Quark
    event.add(/quark:\w+_corundum$/, [
        'Will grow up to four blocks tall if placed deep underground. Will emit particles while growing.',
        ' ',
        'May sprout Corundum Clusters as well.'
    ])
    event.add(/quark:\w+_corundum_cluster/, [
        'Grows occasionally on Corundum Blocks when grown underground.'
    ])
    event.add('quark:bottled_cloud', [
        'Obtained by Right-Clicking a Glass Bottle in the air between Y Levels 126 and 132.'
    ])
    event.add('quark:dragon_scale', [
        'Only drops from Ender Dragons after the first one has been killed.'
    ])
    event.add('quark:slime_in_a_bucket', [
        'Scoop up a small slime in an empty bucket.'
    ])

    // TConstruct
    event.add('tconstruct:dragon_scale', [
        'Drops when the Ender Dragon is killed by an explosion.'
    ])

    // Thermal
    event.add('thermal:bitumen', [
        'Byproduct of refining Oil in a Distillation Tower.'
    ])
    event.add([
        'thermal:blizz_rod',
        'thermal:blizz_spawn_egg'
    ], [
        'The Blizz spawns naturally in cold biomes.',
        ' ',
        'Mechanical Dirt and Nocturnal Powder work wonders for farming them.'
    ])
    event.add([
        'thermal:blitz_rod',
        'thermal:blitz_spawn_egg'
    ], [
        'The Blitz spawns naturally in warm, dry biomes.',
        ' ',
        'Mechanical Dirt and Nocturnal Powder work wonders for farming them.'
    ])
    event.add([
        'thermal:basalz_rod',
        'thermal:basalz_spawn_egg'
    ], [
        'The Basalz spawns naturally in barren biomes, such as Badlands.',
        ' ',
        'Mechanical Dirt and Nocturnal Powder work wonders for farming them.'
    ])

    // Industrial For Going
    event.add([
        'industrialforegoing:infinity_backpack',
        'industrialforegoing:infinity_saw',
        'industrialforegoing:infinity_drill',
        'industrialforegoing:infinity_hammer',
        'industrialforegoing:infinity_trident',
        'industrialforegoing:infinity_nuke',
        'industrialforegoing:infinity_launcher'
    ], [
        'Nine Quintillion is big.',
        'Really big.',
        'You just won\'t believe how vastly hugely mind-bogglingly big it is.'
    ])
    event.add([
        'industrialforegoing:infinity_backpack',
        'industrialforegoing:infinity_saw',
        'industrialforegoing:infinity_drill',
        'industrialforegoing:infinity_hammer',
        'industrialforegoing:infinity_trident',
        'industrialforegoing:infinity_nuke',
        'industrialforegoing:infinity_launcher'
    ], [
        'Unless you plan on sitting here for a few centuries, filling this tool isn\'t possible through conventional means.',
        'Some say the answer lies in Nucleosynthesis instead.'
    ])

    // Immersive Engineering
    event.add('immersiveengineering:logic_circuit', [
        'Used in a Logic Unit to create advanced Redstone Logic.',
        ' ',
        'Crafted in an Engineer\'s Circuit Table. Requires Circuit Backplanes and Lead or Copper Wire. Vacuum Tubes are required for all operators except SET'
    ])

    event.add('immersiveengineering:cloche', [
        'Available Fertilizers:',
        ' ',
        'Industrial Fertilizer: 1.15x',
        'Bone Meal: 1.25x',
        'Glowrose Fertilizer: 1.35x',
        'Phyto-Gro: 1.45x',
        'Mystical Fertilizer: 1.55x'
    ])

})
