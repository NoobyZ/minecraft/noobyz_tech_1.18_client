// priority: 0

console.info('Hide JEI Items!')

onEvent('jei.hide.items', event => {
    // Hide items in JEI here
    // event.hide('minecraft:cobblestone')

    // Quark
    event.hide('quark:gunpowder_sack')
    event.hide(/quark:(potato|carrot|beetroot)_crate/)
    event.hide(/quark:(charcoal)_block/)

    //Mekanism
    event.hide('mekanism:block_charcoal')
    event.hide('mekanism:creative_bin')
    event.hide(Ingredient.of('mekanism:creative_energy_cube').ignoreNBT())
    event.hide(Ingredient.of('mekanism:creative_fluid_tank').ignoreNBT())
    event.hide(Ingredient.of('mekanism:creative_chemical_tank').ignoreNBT())

    // Applied Energistics 2
    event.hide(/ae2:creative_(energy|item|fluid)_cell/)

    // Laziers AE2
    event.hide('lazierae2:etcher')

    // AE2 Things
    event.hide('ae2things:advanced_inscriber')

    // Applied Botanics
    event.hide('appbot:creative_mana_cell')

    // Applied Mekanistics
    event.hide('appmek:creative_chemical_cell')

/*
    // Refined Storage
    event.hide(/refinedstorage:(white|orange|magenta|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_(controller|creative_controller|grid|network_receiver|relay|disk_manipulator|crafter|crafting_monitor|detector)/)
    event.hide(/refinedstorage:(white|orange|magenta|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_(crafting|pattern|fluid)_grid/)
    event.hide(/refinedstorage:(white|orange|magenta|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_(network|wireless)_transmitter/)
    event.hide(/refinedstorage:(white|orange|magenta|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_(crafter|security)_manager/)
    event.hide(/refinedstorage:creative_(portable|wireless)_grid/)
    event.hide(/refinedstorage:creative_wireless_(fluid_grid|crafting_monitor)/)
    event.hide(/refinedstorage:creative_storage_(disk|block)/)
    event.hide(/refinedstorage:creative_fluid_storage_(disk|block)/)
    event.hide('refinedstorage:creative_controller')
    event.hide('refinedstorageaddons:creative_wireless_crafting_grid')

    //Extra Storage
    event.hide(/extrastorage:storagepart_(256k|1024k|4096k|16384k)/)
    event.hide(/extrastorage:disk_(256k|1024k|4096k|16384k)/)
    event.hide(/extrastorage:block_(256k|1024k|4096k|16384k)/)
    event.hide(/extrastorage:storagepart_(16384k|65536k|262144k|1048576k)_fluid/)
    event.hide(/extrastorage:disk_(16384k|65536k|262144k|1048576k)_fluid/)
    event.hide(/extrastorage:block_(16384k|65536k|262144k|1048576k)_fluid/)

    // Cable Tiers
    event.hide(/cabletiers:creative_(exporter|importer|constructor|destructor|disk_manipulator|requester)/)
*/
    // Tinkers` Construct
    event.hide(Ingredient.of('tconstruct:creative_slot').ignoreNBT())

    // Tinkers Reforged
    event.hide(Ingredient.of('tinkers_reforged:great_blade').ignoreNBT())
    event.hide(Ingredient.of('tinkers_reforged:large_round_plate').ignoreNBT())
    event.hide(Ingredient.of('tinkers_reforged:frying_pan').ignoreNBT())
    event.hide(Ingredient.of('tinkers_reforged:greatsword').ignoreNBT())

    // Prefab (Haus-Vorlagen)
    event.hide(Ingredient.of('prefab:item_creative_bulldozer').ignoreNBT())

    // Cyclic
    event.hide(/cyclic:(energy|item|fluid)_pipe/)
    event.hide('cyclic:pipe')
    event.hide('cyclic:trash')
    event.hide(/cyclic:(battery|item)_infinite/)

    // Mystical Agriculture
    event.hide(/mysticalagriculture:(rubber|mithril|tungsten|chrome|iridium|titanium|platinum)_(seed|essence)/)
    event.hide('mysticalagriculture:creative_soulium_dagger')
    event.hide('mysticalagradditions:creative_essence')

    // Thermal
    event.hide(/thermal:(rf_coil|fluid_tank|machine_efficiency|machine_catalyst)_creative_augment/)

    // Draconic Evolution
    event.hide(/draconicevolution:creative_(op_capacitor|capacitor)/)

    // Extrem Reactor (Big Reactors)
    event.hide(/bigreactors:reinforced_(reactorcreativewatergenerator|turbinecreativesteamgenerator)/)
    event.hide('bigreactors:basic_turbinecreativesteamgenerator')

    // Immersive Engineering
    event.hide('immersiveengineering:capacitor_creative')

    // RFTools
    event.hide('rftoolspower:dimensionalcell_creative')
    event.hide('rftoolsutility:creative_screen')

    // Pipez
    event.hide('pipez:infinity_upgrade')

    // Botania
    event.hide('botania:creative_pool')
    event.hide('botania:infrangible_platform')
//    event.hide('lens_storm')
//    event.hide('mana_tablet')
    event.hide('botania:corporea_spark_creative')

    // Botany Pots Tiers
    event.hide('botanypotstiers:creative_terracotta_botany_pot')
    event.hide('botanypotstiers:creative_terracotta_hopper_botany_pot')
    event.hide(/botanypotstiers:creative_(white|orange|magenta|light_blue|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_terracotta_botany_pot/)
    event.hide(/botanypotstiers:creative_(white|orange|magenta|light_blue|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_terracotta_hopper_botany_pot/)
    event.hide(/botanypotstiers:creative_(white|orange|magenta|light_blue|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_glazed_terracotta_botany_pot/)
    event.hide(/botanypotstiers:creative_(white|orange|magenta|light_blue|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_glazed_terracotta_hopper_botany_pot/)
    event.hide(/botanypotstiers:creative_(white|orange|magenta|light_blue|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_concrete_botany_pot/)
    event.hide(/botanypotstiers:creative_(white|orange|magenta|light_blue|yellow|lime|pink|gray|light_gray|cyan|purple|blue|brown|green|red|black)_concrete_hopper_botany_pot/)

    // Building Gadgets
    event.hide('buildinggadgets:construction_paste_container_creative')

    // Create
    event.hide(/create:creative_(motor|fluid_tank|crate|blaze_cake)/)
    event.hide('create:handheld_worldshaper')

    // Create Crafts & Additions
    event.hide('createaddition:creative_energy')

    // Little Logistics
    event.hide('littlelogistics:creative_capacitor')

    // PneumaticCraft: Repressurized
    event.hide(/pneumaticcraft:creative_(upgrade|compressor|compressed_iron_block)/)

    // Titanium
    event.hide('titanium:creative_generator')

})
